# Project No. 2
# Author: Alex Marsh
# This is the main function for the boggle board. It imports the methods from the BoggleBoard class,
# and passes the parameters to the Boggle Board file
from BoggleBoard import BoggleBoard


def main():
    seed1 = int(input('Enter a seed:'))
    start = BoggleBoard(seed1)
    start.make_board()

    playerinput1 = input('What word: ').upper()

    start.filled_board(playerinput1)
    start.is_palindorme(playerinput1)


main()
