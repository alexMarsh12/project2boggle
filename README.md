# project2boggle
project 2 boggle board
# Description: This file is the driving code for the boggle board. From the main
# file, we receive a seed inputted by the player: This seed allows the random function to automatically choose
# characters for the random.choose function and this also allows us to test the set-up. This function relies on the
# make board function to generate a 2d array of random characters, and using the format in the project file we can
# generate a boggle board. From the main file, the player can read the board printed off from function,
# and the player can input a word they see in the boggle board.
# The file recursively checks the inputted word, and processes it in two ways.
# One way is to check to see if the board contains the word, it will check the first character of the
# Player inputted word, and will check the board for that first character, and then the neighbors
# of each subsequent character to fulfill that word. Otherwise, the board will reprint and give an error message
# to the player. However, I had trouble implementing that algorithm, so while I can work it out
# in words, I was not able to successfully implement this, as shown from the excessive self.__ methods, in which
# I was going to use the grid array as a second array to use as a position guide, which was not successful.
# The last part of the function will check the inputted word to see if it's a palindrome, through recursion
# it will check if the first character of said word, is the same as the last.
# Another error I have encountered is that when selecting characters from the player input, the format is messed up
