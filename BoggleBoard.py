# Project No. 2
# Author: Alex Marsh
# Description: This file is the driving code for the boggle board. From the main
# file, we receive a seed inputted by the player: This seed allows the random function to automatically choose
# characters for the random.choose function and this also allows us to test the set-up. This function relies on the
# make board function to generate a 2d array of random characters, and using the format in the project file we can
# generate a boggle board. From the main file, the player can read the board printed off from function,
# and the player can input a word they see in the boggle board.
# The file recursively checks the inputted word, and processes it in two ways.
# One way is to check to see if the board contains the word, it will check the first character of the
# Player inputted word, and will check the board for that first character, and then the neighbors
# of each subsequent character to fulfill that word. Otherwise, the board will reprint and give an error message
# to the player. However, I had trouble implementing that algorithm, so while I can work it out
# in words, I was not able to successfully implement this, as shown from the excessive self.__ methods, in which
# I was going to use the grid array as a second array to use as a position guide, which was not successful.
# The last part of the function will check the inputted word to see if it's a palindrome, through recursion
# it will check if the first character of said word, is the same as the last.
# Another error I have encountered is that when selecting characters from the player input, the format is messed up
import string
import random


class BoggleBoard:
    def __init__(self, seed1):
        self.__seed = seed1
        random.seed(seed1)
        self.__makeboard = [[random.choice(string.ascii_uppercase) for i in range(4)] for i in range(4)]
        self.__grid = [[-1, 0], [1, 0], [1, 1],
                       [1, -1], [-1, -1], [-1, 1],
                       [0, 1], [0, -1]]
        self.__row = None
        self.__column = None

    def get_seed(self):
        return self.__seed

    def set_seed(self, newseed):
        self.__seed = newseed

    def new_seed(self):
        seed1 = int(input('Enter a seed:'))
        random.seed(seed1)
        return seed1

    def make_board(self):

        for i in range(4):
            for k in range(1):
                print("+---+ +---+ +---+ +---+")
                print(
                    f'| {self.__makeboard[i][0]} | | {self.__makeboard[i][1]} | | {self.__makeboard[i][2]} | | {self.__makeboard[i][3]} |')
            print("+---+ +---+ +---+ +---+")

    def filled_board(self, playerinput):

        for i in range(4):
            for k in range(4):
                if self.__makeboard[i][k] in playerinput:
                    self.__makeboard[i][k] = f'<{self.__makeboard[i][k]}>'

                else:
                    self.__makeboard[i][k] = self.__makeboard[i][k]
        print('Are we looking at the same board?')

        for i in range(4):
            for k in range(1):
                print("+---+ +---+ +---+ +---+")
                print(f'| {self.__makeboard[i][0]} | | {self.__makeboard[i][1]} | | {self.__makeboard[i][2]} | | {self.__makeboard[i][3]} |')
            print("+---+ +---+ +---+ +---+")

    def is_palindorme(self, input_string):
        if len(input_string) <= 1 or len(input_string) <= 0:
            return True
        else:
            if len(input_string) > 1:
                if input_string[0] != input_string[-1]:
                    print(input_string, 'is not a palindrome')
                else:
                    if input_string[0] == input_string[-1]:
                        print(input_string, 'is a palindrome')
